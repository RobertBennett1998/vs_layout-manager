﻿namespace Layout_Switcher
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.IO;
    using System.Collections.ObjectModel;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.CommandBars;
    using Microsoft.VisualStudio.Shell;
    using Microsoft.VisualStudio.Shell.Interop;

    /// <summary>
    /// Interaction logic for LayoutManagerControl.
    /// </summary>
    public partial class LayoutManagerControl : UserControl
    {
        public class LayoutFileEntry
        {
            public LayoutFileEntry()
            {

            }
            public LayoutFileEntry(string path)
            {
                var split = path.Split('\\');
                FileName = split[split.Length - 1].Split('.')[0];
                FilePath = path;
            }

            public string FileName
            {
                get;
                set;
            }

            public string FilePath
            {
                get;
                set;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LayoutManagerControl"/> class.
        /// </summary>
        public LayoutManagerControl()
        {
            this.InitializeComponent();
        }

        private ObservableCollection<LayoutFileEntry> _FileEntries = new ObservableCollection<LayoutFileEntry>();
        public ObservableCollection<LayoutFileEntry> FileEntries
        {
            get
            {
                return _FileEntries;
            }
            set
            {
                _FileEntries = value;
            }
        }

        private static string GetLayoutFolderPath()
        {
            string codebase = (typeof(LayoutManager)).Assembly.CodeBase;
            var uri = new Uri(codebase, UriKind.Absolute);
            string path = uri.LocalPath;
            string[] consituents = path.Split('\\');
            path = "";
            for (int i = 0; i < consituents.Length - 1; i++)
                path += consituents[i] + @"\";

            path += @"Layouts\";
            return path;
        }

        private void MyToolWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Initialze();
        }

        private void Initialze()
        {
            FileEntries.Clear();

            if (!Directory.Exists(GetLayoutFolderPath()))
            {
                Directory.CreateDirectory(GetLayoutFolderPath());
            }

            foreach (string s in Directory.GetFiles(GetLayoutFolderPath(), "*.vssettings"))
            {
                FileEntries.Add(new LayoutFileEntry(s));
            }
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            Initialze();
        }

        private void Load_Selected_Click(object sender, RoutedEventArgs e)
        {
            if (LayoutFilesView.SelectedItem != null)
            {
                string cmd = "-import:\"" + ((LayoutFileEntry)LayoutFilesView.SelectedItem).FilePath + "\"";
                LayoutManagerPackage.gDTE.Commands.Raise("{1496A755-94DE-11D0-8C3F-00C04FC2AAE2}", (int)VSConstants.VSStd2KCmdID.ManageUserSettings, cmd, null);
            }
        }

        private void Save_Current_Click(object sender, RoutedEventArgs e)
        {
            var diag = new System.Windows.Forms.SaveFileDialog()
            {
                Filter = "Visual Studio Settings Files (*.vssettings)|*.vssettings|All files (*.*)|*.*",
                FilterIndex = 0,
                RestoreDirectory = true,
                InitialDirectory = GetLayoutFolderPath()
            };

            if (diag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string cmd = "-export:\"" + diag.FileName + "\"";
                LayoutManagerPackage.gDTE.Commands.Raise("{1496A755-94DE-11D0-8C3F-00C04FC2AAE2}", (int)VSConstants.VSStd2KCmdID.ManageUserSettings, cmd, null);
                bool bFlag = false;
                do
                {
                    try
                    {
                        StreamReader reader = new StreamReader(diag.FileName);
                        string s = reader.ReadToEnd();
                        reader.Close();

                        int iStartIndex = s.IndexOf("<UserSettings>");
                        int iEndIndex = s.IndexOf("</UserSettings>") + ((string)"</UserSettings>").Length;

                        string write = s.Substring(iStartIndex, iEndIndex - iStartIndex);

                        StreamWriter writer = new StreamWriter(diag.FileName, false);
                        writer.Write(write);
                        writer.Close();

                        bFlag = true;
                    }
                    catch (IOException)
                    {
                        bFlag = false;
                    }
                } while (bFlag != true);
            }

            Initialze();
        }
    }
}